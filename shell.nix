{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {

    # Installed Packages
    buildInputs = [ 
        pkgs.nodejs-13_x
        pkgs.nur.repos.husseinraoouf.node-packages.gatsby-cli
    ];

    # Enviroment Variables

}